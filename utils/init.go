package utils ;

import (
  "os"
  "log"
)

var (
  RECAPTCHA_KEY string
  RECAPTCHA_SECRET_KEY string
)

const (
  RECAPTCHA_VALIDATION_URL string = "https://www.google.com/recaptcha/api/siteverify"
)

func init() {
  RECAPTCHA_KEY = os.Getenv("RECAPTCHA_KEY")
  if len(RECAPTCHA_KEY) == 0 {
    log.Fatal("RECAPTCHA_KEY was not set")
  }

  RECAPTCHA_SECRET_KEY = os.Getenv("RECAPTCHA_SECRET_KEY")
  if len(RECAPTCHA_SECRET_KEY) == 0 {
    log.Fatal("RECAPTCHA_SECRET_KEY was not set")
  }

}
