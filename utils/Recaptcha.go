package utils ;



import (
  "github.com/gin-gonic/gin"
  "net/http"
  "io/ioutil"
  "log"
  "net/url"
  "encoding/json"
)

type RecaptchaRequest struct {
  Response string `form:"g-recaptcha-response", binding:"required"`
}

type RecaptchaResponse struct {
  Success bool `json:"success"`
}

func ValidRecaptchaRequest_P(ctx *gin.Context) bool {
  var request RecaptchaRequest

  if ctx.Bind(&request) == nil {

    // validate agains recaptcha
    // resp, err := http.PostForm("http://example.com/form",	url.Values{"key": {"Value"}, "id": {"123"}})
    resp, err := http.PostForm(RECAPTCHA_VALIDATION_URL,
                                url.Values{"secret":{RECAPTCHA_SECRET_KEY},
                                           "response":{request.Response},
                                           "remoteip":{ctx.Request.RemoteAddr}})
    if err != nil {
      log.Println("[controllers/utils ValidRecaptchaRequest_P] Unable to post to the Recaptcha servers")
      return false
    } else {
      var response RecaptchaResponse

      b, _ := ioutil.ReadAll(resp.Body)
      log.Println( string(b) )
      err := json.Unmarshal(b, &response)

      log.Println("result: ", response)

      if err != nil {
        return false
      } else {
        return response.Success
      }


    }
  } else {
    log.Println("[controllers/utils ValidRecaptchaRequest_P] Unable to bind request")
    return false
  }

}
