package main ;

import (
	_ "gitlab.com/esantoro/stack-todolist/models"
	"gitlab.com/esantoro/stack-todolist/controllers"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"

	"os"
	"log"
	//"fmt"
)

var (
	/*
	TCP port for HTTP interface.
	Went back from HTTP_PORT to PORT for Heroku compatibility.
	*/
	PORT string
	COOKIE_SIGNING_SECRET string
)


func main() {
	log.Println("[main] Starting up...")

	PORT = os.Getenv("PORT")
	if len(PORT) == 0 {
		log.Println("[main] $PORT was not set, defaulting to 5000.")
		PORT = "5000"
	}

	COOKIE_SIGNING_SECRET = os.Getenv("COOKIE_SIGNING_SECRET")
	if len(COOKIE_SIGNING_SECRET) == 0 {
		log.Println("[main] $COOKIE_SIGNING_SECRET was not set, defaulting to 'secret' ")
		COOKIE_SIGNING_SECRET = "SECRET"
	}

	log.Println("[main] Spawning web server interface.")

	COOKIE_STORE := sessions.NewCookieStore([]byte(COOKIE_SIGNING_SECRET))

	ROUTER := gin.Default()

	ROUTER.Static("/static", "./static")

	ROUTER.LoadHTMLGlob("templates/*")
	ROUTER.Use(sessions.Sessions("stack-todolist", COOKIE_STORE))

	ROUTER.GET("/checks", controllers.ChecksController)
	ROUTER.GET("/login", controllers.LoginController)

	ROUTER.GET("/signup", controllers.SignupController)
	ROUTER.POST("/signup", controllers.SignupController)

	// Controller-ize a static HTML page
	ROUTER.GET("/example", controllers.StaticPageController("example.html"))

	ROUTER.POST("/push", controllers.PushController)
	ROUTER.GET( "/push", controllers.PushController)

	// Tests
	// ROUTER.GET("/push", controllers.StaticPageController("push.html"))
	// ROUTER.GET("/bootstrap", controllers.StaticPageController("layout.html"))

	ROUTER.POST("/logout", controllers.LogoutController)
	ROUTER.GET("/pop", controllers.PopController)


	ROUTER.POST("/auth", controllers.AuthController)
	ROUTER.GET("/", controllers.RootController)
	ROUTER.Run(":"+PORT)
}
