STACK-TODOLIST
===============

+ Want to save an article for later?
+ Worried you'll accumulate tons of articles but you'll never actually read any of them ?

## The stack!

You can push as many articles as you want on the stack (\*)

__BUT__: you must be careful!

Once you pop an article off the stack, it's lost.

So whenever you feel you have twenty spare minutes, just pop an article off the stack, focus on it and read it. But make sure you are focused, because that link is not going on the stack again!!!







(\*) Technical limitations may apply.
