package models ;

import (
  // "time"
  "bytes"
  // "fmt"
  "github.com/jinzhu/gorm"
  "golang.org/x/crypto/bcrypt"
  "errors"
  "log"

)

type User struct {
  gorm.Model
  // ID int `gorm:"primary_key not null"`

  Username string `sql:"not null;unique"`
  PasswordHash string `sql:"not null"`
  Email string
}

func NewUser(username, password, email string) User {
  return User{Username:username, PasswordHash:HashPassword(password), Email:email}
}

func UserByUsername(username string) (User,error) {
  u := new(User)

  DB.Where("username = ?", username).First(&u)

  if u.ID == 0 { // not found
    return User{}, errors.New("User not found")
  } else {
    return *u, nil
  }
}


func ExistsUserForUsername_P(username string) bool {
  var u User

  DB.Where("username = ?", username).First(&u)

  /*
    u.ID == 0 => user does not exist (0 is default value)
    u.ID != 0 => user does exist and u.ID is its user-id

    u.ID != 0 => user exists ?
  */
  return u.ID > 0
}


func HashPassword(password string) string{
  password_bytes := bytes.NewBufferString(password).Bytes()

  hash, err := bcrypt.GenerateFromPassword(password_bytes, 10)
  if err != nil {
    panic(err)
  }

  return string(hash)
}

func VerifyPasswordForUser(user *User, password_attempt string) bool {
  // for later reference, check password with:
  // err = bcrypt.CompareHashAndPassword(hashedPassword, password) ;
  // err == nil  => match
  // err != nil  => mismatch

  password_attempt_bytes := bytes.NewBufferString(password_attempt).Bytes()
  user_password_hash_bytes := bytes.NewBufferString(user.PasswordHash).Bytes()

  return bcrypt.CompareHashAndPassword(user_password_hash_bytes, password_attempt_bytes) ==  nil
}

func (u *User) Save() bool {
  DB.Create(u) ;

  log.Println("[models/User Save() --- u.ID:", u.ID)
  return DB.NewRecord(u) != true
}

func (this *User) PopLink() Link {
  var l Link
  DB.Where("owner_id = ?", this.ID).Order("created_at asc").Find(&l)
  DB.Delete(&l)

  return l
}
