package models ;

import (
  "testing"
)

const (
  TEST_USER_USERNAME = "znpy"
  TEST_USER_PASSWORD = "password"
  TEST_USER_WRONG_PASSWORD = "drowssap"
)

func TestCreateCose(t *testing.T) {

  var user User = NewUser(TEST_USER_USERNAME, TEST_USER_PASSWORD, "manu@santoro.tk")

  t.Log("User is a new record? ", DB.NewRecord(&user))
  t.Log(DB)

  if DB.Create(&user) ; DB.NewRecord(&user) {
    t.Log(user.ID)
    t.Fatal("Failed to insert User-struct into database.")
  } else {
    t.Log("Tutto okay.")
  }


  t.Log("User is a new record? ", user.ID)
  t.Log("User is a new record? ", DB.NewRecord(user))

}

func TestExistsExistingUser(t *testing.T) {
  var exists bool = ExistsUserForUsername_P(TEST_USER_USERNAME)

  if ! exists {
    t.Fatal("Failed to find existing user")
  }
}

func TestExistsNONExistingUser(t *testing.T) {
  var exists bool = ExistsUserForUsername_P("qualcuno")

  if exists {
    t.Fatal("Found a non-existing user.")
  }
}




func TestVerifyRightPassword(t *testing.T) {
  var u User

  DB.First(&u)

  if valid_password_p := VerifyPasswordForUser(&u, TEST_USER_PASSWORD); valid_password_p {
    //
    t.Log("TestVerifyRightPassword should pass and this text is not supposed to be seen.")
  } else {
    t.Fatal("Failed to verify matching passwords")
  }


}

func TestVerifyWrongPassword(t *testing.T) {
  var u User

  DB.First(&u)

  if valid_password_p := VerifyPasswordForUser(&u, TEST_USER_WRONG_PASSWORD); valid_password_p {
    //
    t.Fatal("Failed to verify mismatching passwords: the system accepted a wrong password for good.")
  } else {
    t.Log("TestVerifyRightPassword should pass and this text is not supposed to be seen.")
  }


}


func TestDeleteCose(t *testing.T) {
  users := make([]User, 16)

  DB.Find(&users)
  t.Log("Lunghezza:", len(users))

  DB.Delete(&users[0])

  DB.Find(&users)
  t.Log("Lunghezza:", len(users))

  if len(users) > 0 {
    t.Fail()
  }
}
