package models ;

import (
  "github.com/jinzhu/gorm"
)

type Link struct {
  gorm.Model

  Owner User
  OwnerID uint
  LinkUrl string `sql:"type:text;not null"`
  // LinkDescription string `sql:"type:text"`
}


func NewLink(owner User, link string) Link {
  return Link{Owner:owner, OwnerID:owner.ID, LinkUrl:link}
}


func (this *Link) Save() bool {
  DB.Create(this)

  return DB.NewRecord(this) != true
}


func (this *Link) Delete() {
  return
}
