package models ;

import (
	"os"
	"log"

	"github.com/jinzhu/gorm"
  _ "github.com/lib/pq"

)

var (
	DATABASE_URL string
	DB_TYPE string
	DB gorm.DB								/* package-wide database connection */
	err error
)

func init() {
	log.Println("[models/init] Beginning models initialization") ;

	/* Read variables from environment variables: */
	DATABASE_URL = os.Getenv("DATABASE_URL")
	if len(DATABASE_URL) == 0 {
		log.Fatal("[models/init] $DATABASE_URL was not set, exiting.")
	}

	DB_TYPE = os.Getenv("DB_TYPE")
	if len(DB_TYPE) == 0 {
		log.Fatal("[models/init] $DB_TYPE was not set, exiting.")
	} else if DB_TYPE == "postgresql" {
		log.Println("[models/init] $DB_TYPE was set to 'postgresql', fixing to 'postgres'")
		DB_TYPE = "postgres"
	}

	/* Initialize the database connections here. */
	DB, err = gorm.Open(DB_TYPE, DATABASE_URL)
	if err != nil {
		log.Panic("[models/init] unable to establish database connection", err)
	}
	if DB.DB().Ping() == nil {
		log.Println("[models/init] Database successfully pinged.")
	}

	log.Println("[models/init] Auto-migrating tables...")
	DB.AutoMigrate(&User{})
	DB.AutoMigrate(&Link{})


	log.Println("[models/init] Models initialization complete.")
}
