package controllers ;

import (
  "net/http"
  "github.com/gin-gonic/gin"
)

func StaticPageController(html_page string) func(*gin.Context) {

  return func(ctx *gin.Context) {
    ctx.HTML(http.StatusOK, html_page, gin.H{})
  }
}
