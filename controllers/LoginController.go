package controllers ;

/*
  Login controller: bound to /login

  checks if the user is alredy authenticated (check cookies).
  If user is authenticated, redirect to /. Otherwise, display a login page.

*/

import (
  "github.com/gin-gonic/contrib/sessions"
  "github.com/gin-gonic/gin"
  "net/http"
)

func LoginController(ctx *gin.Context) {
  session := sessions.Default(ctx)
  if session.Get("auth_user") != nil {
    ctx.Redirect(http.StatusSeeOther, "/push")
    return
  }

  danger_flashes  := session.Flashes("danger_flash")
  warning_flashes := session.Flashes("warning_flash")
  info_flashes    := session.Flashes("info_flashes")

  session.Delete("danger_flash")
  session.Delete("warning_flash")
  session.Delete("info_flash")
  session.Save()

  ctx.HTML(200, "login.html", gin.H{"info_flashes":info_flashes, "danger_flashes":danger_flashes, "warning_flashes":warning_flashes})
  return
}
