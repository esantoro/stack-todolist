package controllers;

import (
  "net/http"
  "github.com/gin-gonic/gin"
)

func RootController(ctx *gin.Context) {
  ctx.Redirect(http.StatusSeeOther, "/login")
  return

  // ctx.String(200, "Root controller")
}
