package controllers ;


import (
  "github.com/gin-gonic/contrib/sessions"
  "github.com/gin-gonic/gin"
  "net/http"

  "gitlab.com/esantoro/stack-todolist/models"
  "fmt"
)

func PopController(ctx *gin.Context) {
  session := sessions.Default(ctx)

  auth_user := fmt.Sprintf("%s", session.Get("auth_user"))


  if auth_user == "" {
    ctx.String(http.StatusUnauthorized, "/login")
    return
  }
  // else ...

  user, err := models.UserByUsername(auth_user)
  if err != nil {
    ctx.String(http.StatusUnauthorized, "Unauthorized.")
    return
  }
  // else...

  link := user.PopLink()

  ctx.Redirect(http.StatusSeeOther, link.LinkUrl)
  return
}
