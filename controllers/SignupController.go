package controllers;

import (
  "github.com/gin-gonic/contrib/sessions"
  "github.com/gin-gonic/gin"
  "gitlab.com/esantoro/stack-todolist/models"
  "gitlab.com/esantoro/stack-todolist/utils"
  "net/http"
  "log"
)

const (
  RECAPTCHA_VALIDATION_URL = "https://www.google.com/recaptcha/api/siteverify"
)

type SignupRequest struct {
  Username  string `form:"username", binding:"required"`
  Password1 string `form:"password1", binding:"required"`
  Password2 string `form:"password2", binding:"required"`
  Email     string `form:"email",`
  Recaptcha string `form:"g-recaptcha-response", binding:"required"`

}


func SignupController(ctx *gin.Context) {
  session := sessions.Default(ctx)

  if ctx.Request.Method == "GET" {

    flashes := session.Flashes("error_msg")
    log.Println("len(flashes)", len(flashes))

    if len(flashes) > 0 {
      session.Delete("error_msg")
      session.Save()
      ctx.HTML(http.StatusOK, "signup.html", gin.H{"msg": flashes[0]})
      return
    }

    session.Delete("error_msg")
    session.Save()
    ctx.HTML(http.StatusOK, "signup.html", gin.H{})
    return


  } else if ctx.Request.Method == "POST" {
    // Handle POST requests

    var request SignupRequest
    if ctx.Bind(&request) == nil {
      var user models.User

      // let's check if the password fields are ok
      if (request.Password1 != request.Password2) {
        log.Println("setting flashes")

        session.AddFlash("The password fields must coincide.", "error_msg")
        session.Save()

        ctx.Redirect(http.StatusSeeOther, "/signup")
        return
      }

      // validate Recaptcha
      if ! utils.ValidRecaptchaRequest_P(ctx) {
        session.AddFlash("Please fill the CAPTCHA correctly", "error_msg")
        session.Save()

        ctx.Redirect(http.StatusSeeOther, "/signup")
        return
      }

      user = models.NewUser(request.Username, request.Password1, request.Email)

      if user.Save() {
        ctx.String(200, "Saved!") ; return
      } else {
        ctx.String(500, "Not saved") ; return
      }


    } else {
      // TODO: set some error message
      session.AddFlash("Something went wrong, please check your inputs and try again.", "warning_flash")
      ctx.Redirect(http.StatusSeeOther, "/signup")
      return
    }
  } else {
    ctx.String(http.StatusMethodNotAllowed, "Method not supported.")
  }
}
