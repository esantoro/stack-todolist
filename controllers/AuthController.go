package controllers ;

/*
AuthController: basically, this thing checks password.

This controller is supposed to receive _only_ POST requests coming from /login.
*/


import (
  "github.com/gin-gonic/contrib/sessions"
  "github.com/gin-gonic/gin"
  // "github.com/gin-gonic/gin/binding"

  "gitlab.com/esantoro/stack-todolist/models"

  "net/http"
  "log"
)

type LoginAttempt struct {
  Username string `form:"username" binding:"required"`
  Password string `form:"password" binding:"required"`
}


func AuthController(ctx *gin.Context) {
  session := sessions.Default(ctx)

  if session.Get("auth_user") != nil {
    ctx.Redirect(http.StatusSeeOther, "/push")
    return
  }


  var form LoginAttempt

  if ctx.Bind(&form) == nil {
    user, err := models.UserByUsername(form.Username)

    if err != nil {
      session.AddFlash("User not found or wrong password. Please try again.", "danger_flash")
      session.Save()
      // ctx.String(200, "(2) User not found")
      ctx.Redirect(http.StatusSeeOther, "/login")
      log.Println("User exists? ", models.ExistsUserForUsername_P(form.Username))
      return
    } else {
      if models.VerifyPasswordForUser(&user, form.Password) {
        session.Set("auth_user", user.Username)
        session.Save()
        ctx.Redirect(http.StatusSeeOther, "/push")
        return
        // ctx.String(200, "Congrats, right password!")
      } else {
        ctx.String(400, "Bad password!")
      }

    }
  } else {
    // error-handling
    session.AddFlash("Something went wrong, please check your inputs and try again.", "warning_flash")
    ctx.String(200, "(1) User not found or wrong password, try again.")
  }

}
