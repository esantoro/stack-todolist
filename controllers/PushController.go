package controllers ;


import (
  "github.com/gin-gonic/contrib/sessions"
  "github.com/gin-gonic/gin"

  "gitlab.com/esantoro/stack-todolist/models"

  "net/http"
)

type PushRequest struct {
  Link string `form:"link" binding:"required"`
}

func PushController(ctx *gin.Context) {
  session := sessions.Default(ctx)
  auth_user := session.Get("auth_user")




  if ctx.Request.Method == "GET" {
    if auth_user == "" {
      /* TODO: display and delete flashes */
      ctx.Redirect(http.StatusSeeOther, "/login")
      return
    }

    danger_flashes  := session.Flashes("danger_flash")
    warning_flashes := session.Flashes("warning_flash")
    info_flashes    := session.Flashes("info_flashes")
    success_flashes := session.Flashes("success_flashes")

    session.Delete("danger_flash")
    session.Delete("warning_flash")
    session.Delete("info_flash")
    session.Delete("success_flashes")
    session.Save()


    ctx.HTML(http.StatusOK, "push.html", gin.H{"username":auth_user,
                                              "danger_flashes":danger_flashes,
                                              "warning_flashes":warning_flashes,
                                              "info_flashes":info_flashes,
                                              "success_flashes":success_flashes})
    return



  } else if ctx.Request.Method == "POST" {
    if auth_user == "" {
      /* TODO: display and delete flashes */
      ctx.String(http.StatusUnauthorized, "Auth first.")
      return
    }


    var request PushRequest

    if ctx.Bind(&request) == nil {
      /* TODO: validare il link contro qualcosa in net/url.URL */
      user, _ := models.UserByUsername(auth_user.(string))
      link := models.NewLink(user, request.Link)
      if link.Save() {
        session.AddFlash("Link pushed", "success_flash")
        session.Save()
        ctx.Redirect(http.StatusSeeOther, "/push")
        return
      } else {
        session.AddFlash("Somethign went wrong", "danger_flash")
        session.AddFlash(request.Link, "wrong_link")
        session.Save()

        ctx.Redirect(http.StatusSeeOther, "/push")
      }

    } else {
      session.AddFlash("Wrong input, please try again", "danger_flash")
      session.Save()
      ctx.Redirect(http.StatusSeeOther, "/push")
    }

    ctx.String(http.StatusOK, "Not implemented yet.")
    return






  } else {
    ctx.String(http.StatusOK, "Method not allowed.")
  }

}
