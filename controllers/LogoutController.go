package controllers ;

import (
  "github.com/gin-gonic/contrib/sessions"
  "github.com/gin-gonic/gin"
  "net/http"
)

func LogoutController(ctx *gin.Context) {
  session := sessions.Default(ctx)
  session.Clear()
  session.Save()
  ctx.Redirect(http.StatusSeeOther, "/login")
}
